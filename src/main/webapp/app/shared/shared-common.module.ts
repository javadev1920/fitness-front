import { NgModule } from '@angular/core';

import { FitnessSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [FitnessSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [FitnessSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class FitnessSharedCommonModule {}
